Es proyecto utiliza Webdriver.io para inyectar el script de [gremlins.js](https://github.com/marmelab/gremlins.js/blob/master/README.md) a la página de [losestudiantes](https://losestudiantes.co) y realizar un ataque de Monkey testing.

Para ejecutar las pruebas, clone este repositorio y descargue las dependencias con el siguiente comando en su terminal:

```bash
$ npm install
```

Luego para correrlas, también en su terminal:

```bash
$ npm test
```

Se implementa el filtro de click con y se filtra tambien solo los Componentes Visibles

```bash
window.gremlins.species.clicker()
    .clickTypes(['click'])
    .canClick(function (element) {
      return ((element.tagName === "button" || element.tagName === "a") && !element.hidden);
    });
```
Se implementa el filtro de formularios y se filtra tambien solo los Componentes Visibles

```bash
window.gremlins.species.formFiller()
    .canFillElement(function (element) {

      return (
        (
          (element.tagName === "input" &&
            (
              element.attributes['type'] === "text" || element.attributes['type'] === "password" || element.attributes['type'] === "email" || element.attributes['type'] === "number"
            )
          ) ||
          (
            element.tagName === "textarea"
          )
        ) &&
        !element.hidden
      );
    });
```